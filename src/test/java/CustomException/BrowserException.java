package CustomException;

/**
 * @author Баскаков Алексей
 * Кастомное исключение
 */
public class BrowserException extends Exception {

    String browser;
    public BrowserException(String browser) {
        this.browser = browser;
    }

    @Override
    public String toString() {
        return "В папке driver в корне католога, отсутствует драйвер для браузера: " + browser + "!!!!";
    }
}
