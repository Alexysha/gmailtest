package BrowserInterface;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;

import java.io.File;

/**
 * @author Баскаков Алексей
 * Инициализация IEDriver
 */
public class IEDriverInit {
    private static final String URI_WEBDRIVER = "driver/IEDriverServer.exe";

    private IEDriverInit() {}

    /**
     * @return драйвер IE
     */
    public static WebDriver getDriver() {
        InternetExplorerDriverService cds = new InternetExplorerDriverService
                .Builder()
                .usingAnyFreePort()
                .usingDriverExecutable(new File(URI_WEBDRIVER))
                .build();

        return new InternetExplorerDriver(cds);
    }
}
