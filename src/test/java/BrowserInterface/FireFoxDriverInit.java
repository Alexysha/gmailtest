package BrowserInterface;

import POM.Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.GeckoDriverService;

import java.io.File;

/**
 * @author Баскаков Алексей
 * Инициализация firefoxdriver
 */
public class FireFoxDriverInit extends Page {
    private static final String URI_WEBDRIVER = "driver/geckodriver.exe";

    private FireFoxDriverInit() {}

    /**
     * @return драйвер мозиллы
     */
    public static WebDriver getDriver() {
        GeckoDriverService cds = new GeckoDriverService
                .Builder()
                .usingAnyFreePort()
                .usingFirefoxBinary(
                        new FirefoxBinary(
                                new File("C://Program Files (x86)/Mozilla Firefox/firefox.exe")))
                .usingDriverExecutable(new File(URI_WEBDRIVER))
                .build();

        return new FirefoxDriver(cds);
    }
}
