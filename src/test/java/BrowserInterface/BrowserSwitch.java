package BrowserInterface;

import org.openqa.selenium.WebDriver;

/**
 * @author Баскаков Алексей
 * Выбор браузера по конфигу
 */
public class BrowserSwitch {
    private BrowserSwitch() {}

    /**
     * @param nameBrowser - ключевое слово, для идентификации драйвера
     * @return драйвер браузера
     */
    public static WebDriver brSwitch(String nameBrowser) {
        WebDriver driver = null;

        switch (nameBrowser) {
            case "Chrome":
                driver = ChromeDriverInit.getDriver();
                break;
            case "IE":
                driver = IEDriverInit.getDriver();
                break;
            case "FireFox":
                driver = FireFoxDriverInit.getDriver();
        }
        return driver;
    }
}
