package BrowserInterface;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;

import java.io.File;

/**
 * @author Баскаков Алексей
 * Инициализация driverchrome
 */
public class ChromeDriverInit {
    private static final String URI_WEBDRIVER = "driver/chromedriver.exe";

    private ChromeDriverInit() {}

    /**
     * @return драйвер хрома
     */
    public static WebDriver getDriver() {
        ChromeDriverService cds = new ChromeDriverService
                .Builder()
                .usingAnyFreePort()
                .usingDriverExecutable(new File(URI_WEBDRIVER))
                .build();

        return new ChromeDriver(cds);
    }
}
