import CustomException.BrowserException;

import io.qameta.allure.Description;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Title;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * Предтест, обязательно выполнить, перед основным тестом!!!!!!
 */
public class MandatoryInitialTest {

    @Test
    @Title("Проверяет присутствие драйвера браузера в папке /driver")
    public void verifyConfigurationFile()  {
        try (FileInputStream fis = new FileInputStream("config.properties")) {
            File file;
            String browser;
            Properties properties = new Properties();

            properties.load(fis);
            browser = properties.getProperty("browser");

            switch (browser) {
                case "Chrome":
                    file = new File("driver/chromedriver.exe");
                    break;
                case "IE":
                    file = new File("driver/geckodriver.exe");
                    break;
                case "FireFox":
                    file = new File("driver/IEDriverServer.exe");
                    break;
                default:
                    throw new IOException("Неверное имя драйвера браузера");
            }

            if (!file.exists()) throw new BrowserException(browser);

        } catch (BrowserException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
}
