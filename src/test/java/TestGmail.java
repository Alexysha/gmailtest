import BrowserInterface.BrowserSwitch;
import POM.*;
import io.qameta.allure.Description;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Title;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static POM.AccountsGoogleComPage.*;
import static POM.GmailHomePage.inputGmailButton;
import static POM.MyAccountGoogleComPage.*;
import static POM.MyWebElement.*;

public class TestGmail extends Page {
    private final String PROPERTIES_PATH =
            "config.properties";

    private final String URL_GOOGLE_HOME_GMAIL  =
            "https://www.google.com/intl/ru/gmail/about/#";

    private String login, password, browser;
    private WebDriver driver;
    private Properties mProperties = new Properties();

    // Массив pages object
    private final Object[] PAGES = {
            new GmailHomePage(),
            new MyAccountGoogleComPage(),
            new AccountsGoogleComPage()
    };

    @BeforeClass
    public void setUpClass() throws IOException {
        FileInputStream fis = new FileInputStream(PROPERTIES_PATH);
        mProperties.load(fis); // Чтение данных из конфига
        login = mProperties.getProperty("login");
        password = mProperties.getProperty("password");
        browser = mProperties.getProperty("browser");
        driver = BrowserSwitch.brSwitch(browser); // Определяет какой драйвер использовать
        driver.get(URL_GOOGLE_HOME_GMAIL); // Переход по ссылке
    }

    @Test
    @Title("Вход в систему и проверка что почтовый адрес аккаунта соответсвует введённому")
    public void testLoginLoginMailCorrectGmailAddress() {
        MyWebElement.setDriver(driver); // передача драйвера для фабрики
        MyWebElement.setPageFactory(PAGES); // передача pages object для фабрики
        login();
        click(iconAvatarUpperRightCorner); // Клик на иконку аватара
        String mail = getText(nameMailControlWindow); // Получение почтового адреса
        Assert.assertEquals(login, mail); // Проверка адреса почты
    }

    @Test(dependsOnMethods = "testLoginLoginMailCorrectGmailAddress")
    @Title("Тестирующий метод - отправка письма на свой же адрес и проверка что оно успешно отправлено")
    public void sendMailWriteAndSendMailSuccessSend() {
        click(writeButtonMail); // Кнопка написать письмо
        setText(inputFieldWhom, login); // Поле - кому
        setText(inputFieldTheme, "Проверка"); // Поле - тема
        setText(inputTextMessage, "Привет, проверка!!!"); // Поле - текст сообщения
        click(buttonSendMessage); // Кнопка отправки сообщения
        String notify = getText(notifyMailSend); // Получения уведомления об отправлении
        Assert.assertEquals("Письмо отправлено. Просмотреть сообщение", notify); // Проверка что уведомление успешное
    }

    @Test(dependsOnMethods = "sendMailWriteAndSendMailSuccessSend")
    @Title("Тестирующий метод - презагрузка браузера и проверка что письмо пришло")
    public void rebootBrowserCheckMailArrive() {
        driver.quit(); // Закрыть все вкладки
        driver = BrowserInterface.BrowserSwitch.brSwitch(browser);
        MyWebElement.setDriver(driver);
        MyWebElement.setPageFactory(PAGES);
        driver.get(URL_GOOGLE_HOME_GMAIL);
        login();
        String message = getText(firstLetterOnGmailAccount); // Получение текста темы первого сообщения на почте
        Assert.assertEquals("Проверка", message); // Проверка что сообщение пришло
    }

    @AfterClass
    public void tearDownClass() {
        driver.navigate().refresh(); // Перезагрузка страницы
        click(cheboxLetter); // Ставит флажок на первом сообщение на почтовом ящике
        click(buttonDelete); // Удаляет выделенные сообщения
        driver.quit(); // Закрыть все вкладки
    }

    @Title("Метод для входа в систему с главной страницы mail.google.com")
    public void login() {
        click(inputGmailButton); // Нажатие кнопки войти
        setText(field_login, login); // Ввод логина
        click(button_next); // Кнопка далее
        setText(field_password, password); // Ввод пароля
        click(button_next);
    }
}
