package POM;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * @author Алексей Баскаков.
 * Page Object стартовой страницы гугл почты - https://mail.google.com.
 */
public class GmailHomePage extends Page {
    @FindBy(xpath = "//a[text()='Войти']")
    public static WebElement inputGmailButton;
}
