package POM;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * @author Алексей Баскаков.
 * Page Object страницы авторизации на почту гугл.
 */
public class AccountsGoogleComPage extends Page {

    @FindBy(xpath = "//input[@type='email']")
    public static WebElement field_login;

    @FindBy(xpath = "//input[@type='password']")
    public static WebElement field_password;

    @FindBy(xpath = "//span[text()='Далее']")
    public static WebElement button_next;

}
