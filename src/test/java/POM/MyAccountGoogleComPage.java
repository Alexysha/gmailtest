package POM;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * @author Алексей Баскаков.
 * Page Object для страницы почты пользователя.
 */
public class MyAccountGoogleComPage extends Page {

    @FindBy(xpath = "//a[@class='gb_b gb_ib gb_R']")
    public static WebElement iconAvatarUpperRightCorner;

    @FindBy(xpath = "//div[@class='gb_Ib']")
    public static WebElement nameMailControlWindow;

    @FindBy(xpath = "//div[text()='НАПИСАТЬ']")
    public static WebElement writeButtonMail;

    @FindBy(xpath = "//textarea[@name='to']")
    public static WebElement inputFieldWhom;

    @FindBy(xpath = "//input[@name='subjectbox']")
    public static WebElement inputFieldTheme;

    @FindBy(xpath = "//div[@aria-label='Тело письма']")
    public static WebElement inputTextMessage;

    @FindBy(xpath = "//div[text()='Отправить']")
    public static WebElement buttonSendMessage;

    @FindBy(xpath = "//div[@class='vh']")
    public static WebElement notifyMailSend;

    @FindBy(xpath = ".//*[@id=':39']/b[text()='Проверка']")
    public static WebElement firstLetterOnGmailAccount;

    @FindBy(xpath = "//td[@class='oZ-x3 xY']")
    public static WebElement cheboxLetter;

    @FindBy(xpath = "//*[@id=\":5\"]/div/div[1]/div[1]/div/div/div[2]/div[3]")
    public static WebElement buttonDelete;

}
