package POM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

/**
 * @author Баскаков Алексей
 * Реализация действий с элементами с уже встроенным ожиданием отображения элемента
 * Уменьшает дубляж кода
 */
public class MyWebElement extends Page {
    private static WebDriver driver;

    private MyWebElement() {}

    /**
     * Передача драйвера фабрики PageFactory
     * @param driver - веб-драйвер
     */
    public static void setDriver(WebDriver driver) {
        MyWebElement.driver = driver;
    }

    /**
     * Добаления страниц в фабрику PageFactory
     * @param objects - массив page object
     */
    public static void setPageFactory(Object[] objects) {
        for (Object o : objects)
            PageFactory.initElements(driver, o);
    }

    /**
     * Клик по элементу
     * @param element - элемент страницы
     */
    public static void click(WebElement element) {
        WebDriverWaitPage(element, driver, 50);
        element.click();
    }

    /**
     * Ввод текста в элемент
     * @param element - элемент страницы
     * @param text - текст для ввода
     */
    public static void setText(WebElement element, String text) {
        WebDriverWaitPage(element, driver, 50);
        element.sendKeys(text);
    }

    /**
     * Получение текста элемента
     * @param element - элемент страницы
     * @return текст сообщения
     */
    public static String getText(WebElement element) {
        WebDriverWaitPage(element, driver, 50);
        return element.getText();
    }
}
